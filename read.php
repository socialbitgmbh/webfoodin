<?php

$servername = "localhost";
$username = "root";
$password = "db_pw";
$dbname = "foodin";

$conn = new mysqli($servername, $username, $password, $dbname);

$taORsk = rand(0, 1);

if($taORsk == 0)
{
    $sql = "SELECT name FROM taH";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $hauptArray[] = $row;
    }

    $sql = "SELECT name FROM taB";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $beiArray[] = $row;
    }
}
else if($taORsk == 1)
{
    $sql = "SELECT name FROM skH";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $hauptArray[] = $row;
    }

    $sql = "SELECT name FROM skB";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $beiArray[] = $row;
    }
}
else
{
    echo 'AMK';
}

$conn->close();

$url = "https://api.hipchat.com/v2/room/859445/message?auth_token=rwVRNfcYjA0csGI84nWIEEA6v3oDRTK1jYHSC444";
$data_string = '{ "message":"Heute gibt es ('.trim($hauptArray[rand(0, count($hauptArray)-1)]['name']).') mit ('.trim($beiArray[rand(0, count($beiArray)-1)]['name']).')"}';
$c = curl_init();
curl_setopt($c, CURLOPT_URL, $url);
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);                                                                  
curl_setopt($c, CURLOPT_POSTFIELDS, "$data_string");                                                                  
curl_setopt($c, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($c, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data_string))
);

echo curl_exec($c);

header('Location: index.html#start');